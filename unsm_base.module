<?php

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\unsm_base\Util\Icon;

/**
 * Implements hook_theme_suggestions_form_alter().
 */
function unsm_base_theme_suggestions_form_alter(array &$suggestions, array $variables) {
  if ($variables['element']['#form_id'] == 'user_login_form') {
    $suggestions[] = sprintf('%s__%s', $variables['theme_hook_original'], $variables['element']['#form_id']);
  }
}

/**
 * Implements template_preprocess_form().
 */
function unsm_base_preprocess_form(array &$variables) {
  $form_id = $variables['element']['#form_id'];
  if ($form_id == 'user_login_form') {
    $variables['register_form_url'] = Url::fromRoute('user.register');
  }
}

/**
 * Implements hook_ENTITY_TYPE_view() for node entities.
 */
function unsm_base_node_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
  /** @var \Drupal\node\NodeInterface $node */
  $node = $entity;
  if ($view_mode == 'full') {
    if (!empty($build['field_gallery'])) {
      // Add additional PhotoSwipe settings that are not part of the config schema.
      if (!empty($build['field_gallery']['#formatter']) && $build['field_gallery']['#formatter'] == 'photoswipe_field_formatter') {
        // Customize the share buttons, only use Facebook and Twitter.
        $build['field_gallery']['#attached']['drupalSettings']['photoswipe']['options']['shareButtons'] = [
          [
            'id' => 'facebook',
            'label' => t('Share on Facebook'),
            'url' => 'https://www.facebook.com/sharer/sharer.php?u={{url}}',
          ],
          [
            'id' => 'twitter',
            'label' => 'Tweet',
            'url' => 'https://twitter.com/intent/tweet?text={{text}}&url={{url}}',
          ],
        ];
      }
    }
    if ($node->bundle() == 'flex_page' && !empty($build['body'])) {
      $has_empty_body = $node->get('body')->isEmpty() || empty($node->body->value);
      if ($has_empty_body) {
        $build['body']['#access'] = FALSE;
      }
    }
  }
}

/**
 * allowed_values_function callback for 'features' field.
 */
function unsm_base_allowed_icon_values(FieldStorageDefinitionInterface $definition, FieldableEntityInterface $entity = NULL, &$cacheable = TRUE) {
  return Icon::getAllowedValues($definition, $entity, $cacheable);
}

/**
 * Implements hook_form_FORM_ID_alter() for user_login_form.
 */
function unsm_base_form_user_login_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  // Set placeholder attribute.
  $fields = ['name', 'pass'];
  foreach ($fields as $field) {
    $form[$field]['#placeholder'] = $form[$field]['#title'];
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for user_form.
 */
function unsm_base_form_user_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if (isset($form['language'])) {
    // Hide language settings.
    $form['language']['#access'] = FALSE;
  }
}

/**
 * Implements hook_preprocess_email_wrap().
 */
function unsm_base_preprocess_email_wrap(&$variables) {
  $base_url = \Drupal::request()->getSchemeAndHttpHost();

  // Convert the whole message body. Returns string.
  $converted = Html::transformRootRelativeUrlsToAbsolute($variables['body'], $base_url);

  // Convert body text to Markup class.
  $variables['body'] = Markup::create($converted);
}

/**
 * Implements hook_page_attachments_alter().
 */
function unsm_base_page_attachments_alter(array &$attachments) {
  foreach ($attachments['#attached']['html_head_link'] as $key => $value) {
    // remove the hreflang tags for the disabled languages
    if (isset($value[0]['hreflang']) && isset($value[0]['rel']) && $value[0]['rel'] == 'alternate') {
      unset($attachments['#attached']['html_head_link'][$key]);
    }
  }

  // Workaround for https://www.drupal.org/project/metatag/issues/2968507.
  // "HTTP Link Tag URLs with multiple parameters have & encoded to &amp;".
  // @todo remove, whenever this is solved within Metatag and/or Core.
  if (isset($attachments['#attached']['http_header'])) {
    foreach ($attachments['#attached']['http_header'] as $key => $items) {
      foreach ($items as $key1 => $item) {
        if (strpos($item, 'canonical') !== FALSE) {
          $attachments['#attached']['http_header'][$key][$key1] = str_replace('&amp;', '&', $item);
        }
      }
    }
  }

  // Workaround for https://www.drupal.org/project/drupal/issues/2968558.
  // "Canonical (and other) links with multiple query parameters have ampersand encoded".
  // @todo remove, whenever this is solved within Core.
  if (isset($attachments['#attached']['html_head'])) {
    foreach ( $attachments['#attached']['html_head'] as $key => $item ) {
      if ( in_array("canonical_url", $item)) {
        $href = $item[0]['#attributes']['href'];
        $attachments['#attached']['html_head'][$key][0] = [
          '#type' => 'inline_template',
          // Formatted this way to include a new line.
          '#template' => "{{ href|raw }}",
          '#context' => [
            'href' => '<link rel="canonical" href="' . $href . '"/>',
          ]
        ];
      }
    }
  }
}

/**
 * Implements hook_preprocess_pager().
 */
function unsm_base_preprocess_pager(&$pager) {
  // We want to add prev/next metadata to (Views) pagers. Using the preprocessor
  // should be seen as workaround. We tried first to use hook_views_pre_render()
  // where we would have easily collected the information, if a pager was used,
  // as well as information about current page, total items, etc. However we
  // didn't see some kind of render array where could have attached the meta
  // data to. Passing around global variables wasn't the solution we were
  // seeking for.
  // Next, we tried hook_metatags_alter(), but this seems to be called too early
  // as we didn't have any pager information set in our view.
  // So, we decided to go for this approach, as suggested in:
  // @see https://www.drupal.org/project/drupal/issues/1567684#comment-9267045

  if (!isset($pager['current'])) {
    return;
  }

  $current_path = \Drupal\Core\Url::fromRoute('<current>', [], ['absolute' => TRUE]);
  $path = $current_path->toString();

  // Correct for first page actually being page 0.
  $current_page = $pager['current'] - 1;
  // Parse the last page link to get the total number of pages.
  // @todo There's the first possible problem here. What if the query parameter
  // isn't named "page"? Should we go for more flexible regex? What if, we have
  // multiple pagers on the same page? Nevertheless this default use case is
  // absolutely enough for this project.
  $total_pages = isset($pager['items']['last']) ? preg_replace('/^.*page=(\d+).*$/', '$1', $pager['items']['last']['href']) : $current_page;

  // Use the base path if on page 2 otherwise `page={{current_page-1}}`.
  $prev_href = ($current_page == 1 ? $path : ($current_page > 1 ? $path . '?page=' . ($current_page-1) : NULL));
  $next_href = $current_page < $total_pages ? $path . '?page=' . ($current_page + 1) : NULL;

  // Add The prev rel link.
  if ($prev_href) {
    $pager['#attached']['html_head_link'][] = [
      [
        'rel' => 'prev',
        'href' => $prev_href,
      ],
      TRUE,
    ];
  }

  // Add the next rel link.
  if ($next_href) {
    $pager['#attached']['html_head_link'][] = [
      [
        'rel' => 'next',
        'href' => $next_href,
      ],
      TRUE,
    ];
  }
}

/**
 * Implements hook_mail_alter().
 */
function unsm_base_mail_alter(array &$message) {
  $allowed_from = \Drupal::config('system.site')->get('mail');
  $msg_from = $message['from'];
  if ($msg_from != $allowed_from) {
    if (empty($message['headers']['Reply-to'])) {
      $message['headers']['Reply-to'] = $msg_from;
    }
    $message['from'] = $allowed_from;
    $message['headers']['From'] = str_replace($msg_from, $allowed_from, $message['headers']['From']);
    $message['headers']['Return-Path'] = $allowed_from;
    $message['headers']['Sender'] = $allowed_from;
  }
}

/**
 * Implement hook_agorasocial_fontawesome_mapping_alter().
 */
function unsm_base_agorasocial_fontawesome_mapping_alter(array &$icon_mapping) {
  if (isset($icon_mapping['facebook'])) {
    $icon_mapping['facebook'] = 'facebook-f';
  }
  if (isset($icon_mapping['linkedin'])) {
    $icon_mapping['linkedin'] = 'linkedin-in';
  }
}
