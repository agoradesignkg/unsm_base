<?php

namespace Drupal\unsm_base\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides the Sendinblue iframe subscription form for customer newsletter.
 *
 * @Block(
 *   id = "sendinblue_iframe_customer",
 *   admin_label = @Translation("Sendinblue: unsinn.de Customer newsletter subscription form"),
 *   category = "Newsletter"
 * )
 */
class SendinBlueIframeSubscriptionCustomer extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $url = 'https://ba98b534.sibforms.com/serve/MUIEAKfK2TBlCKufupO7HB-HyzXw6WZzTBI-ZBtd2YcQ1P3upg0R2-Pp7tpqgN4rQcj8uQkVnb6x6GOXOMHF6sSDJfgKBdOMesGforAqdNBviRmQTbFg7sGHIDr1WM23DqWdAZG9UPMIDaDF4a_qtoYgJvg_09I5n7LwD6qhyeXG_wGUle572bRz5ak9sbSOrDlPr9ukSft4x0Ww';
    $text = 'Abonnieren Sie den UNSINN-Newsletter.';
    $markup = sprintf('<p>%s</p>', $text);
    $markup .= sprintf('<a href="%s" target="_blank" rel="noopener" class="button">zur Anmeldung</a>', $url);

    return [
      'newsletter_subscription' => [
        '#markup' => $markup,
        '#allowed_tags' => ['p', 'a'],
      ],
    ];
  }

}
