<?php

namespace Drupal\unsm_base\Plugin\views\argument;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\taxonomy\Plugin\views\argument\Taxonomy;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Argument handler for term aliases.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("term_alias")
 */
class TermAlias extends Taxonomy {

  /**
   * The default database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityStorageInterface $term_storage, Connection $database) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $term_storage);

    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('taxonomy_term'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setArgument($arg) {
    $terms = $arg != 'all' ? $this->findMatchingTermsByAliasSuffix($arg) : [];
    if ($terms) {
      $term = reset($terms);
      $arg = $term->id();
    }
    return parent::setArgument($arg);
  }

  /**
   * Finds matching taxonomy terms by URL alias suffix.
   *
   * We use the term "alias suffix" here for the part after the last '/' in the
   * path alias.
   *
   * @param $alias
   *   The alias to lookup.
   *
   * @return \Drupal\taxonomy\TermInterface[]
   *   An array of matching taxonomy terms.
   */
  protected function findMatchingTermsByAliasSuffix($alias) {
    if (empty($alias)) {
      return [];
    }
    $select = $this->database->select('path_alias', 'ua');
    $select->condition('path', '/taxonomy/term/%', 'LIKE');
    $select->condition('alias', '/%/' . $this->database->escapeLike($alias), 'LIKE');
    $results = $select
      ->fields('ua')
      ->orderBy('ua.id', 'DESC')
      ->execute()
      ->fetchAllAssoc('id');
    if (empty($results)) {
      return [];
    }
    $tids = [];
    foreach ($results as $result) {
      $source_parts = explode('/', $result->path);
      $tids[] = end($source_parts);
    }
    /** @var \Drupal\taxonomy\TermInterface[] $terms */
    $terms = $this->termStorage->loadMultiple($tids);
    return $terms;
  }

}
