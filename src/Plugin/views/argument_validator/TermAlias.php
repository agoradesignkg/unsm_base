<?php

namespace Drupal\unsm_base\Plugin\views\argument_validator;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Path\AliasStorage;
use Drupal\views\Plugin\views\argument_validator\Entity;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Validates whether a term alias is a valid term argument.
 *
 * @ViewsArgumentValidator(
 *   id = "taxonomy_term_alias",
 *   title = @Translation("Taxonomy term alias"),
 *   entity_type = "taxonomy_term"
 * )
 */
class TermAlias extends Entity {

  /**
   * The default database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The taxonomy term storage.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * Constructs an TermAlias object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, Connection $database) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_type_bundle_info);

    $this->database = $database;
    // Not handling exploding term names.
    $this->multipleCapable = FALSE;
    $this->termStorage = $entity_type_manager->getStorage('taxonomy_term');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validateArgument($argument) {
    $terms = $this->findMatchingTermsByAliasSuffix($argument);

    if (empty($terms)) {
      return FALSE;
    }

    // Not knowing which term will be used if more than one is returned check
    // each one.
    foreach ($terms as $term) {
      if (!$this->validateEntity($term)) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Finds matching taxonomy terms by URL alias suffix.
   *
   * We use the term "alias suffix" here for the part after the last '/' in the
   * path alias.
   *
   * @param $alias
   *   The alias to lookup.
   *
   * @return \Drupal\taxonomy\TermInterface[]
   *   An array of matching taxonomy terms.
   */
  protected function findMatchingTermsByAliasSuffix($alias) {
    if (empty($alias)) {
      return [];
    }
    $select = $this->database->select('path_alias', 'ua');
    $select->condition('source', '/taxonomy/term/%', 'LIKE');
    $select->condition('alias', '/%/' . $this->database->escapeLike($alias), 'LIKE');
    $results = $select
      ->fields('ua')
      ->orderBy('ua.pid', 'DESC')
      ->execute()
      ->fetchAllAssoc('pid');
    if (empty($results)) {
      return [];
    }
    $tids = [];
    foreach ($results as $result) {
      $source_parts = explode('/', $result->source);
      $tids[] = end($source_parts);
    }
    /** @var \Drupal\taxonomy\TermInterface[] $terms */
    $terms = $this->termStorage->loadMultiple($tids);
    return $terms;
  }

}
