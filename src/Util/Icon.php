<?php

namespace Drupal\unsm_base\Util;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

class Icon {

  /**
   * Provide the allowed values for an icons field.
   *
   * Used in our callback for options_allowed_values().
   *
   * That function will be called:
   *  - either in the context of a specific entity, which is then provided as the
   *    $entity parameter,
   *  - or for the field generally without the context of any specific entity or
   *    entity bundle (typically, Views needing a list of values for an exposed
   *    filter), in which case the $entity parameter is NULL.
   * This lets the callback restrict the set of allowed values or adjust the
   * labels depending on some conditions on the containing entity.
   *
   * For consistency, the set of values returned when an $entity is provided
   * should be a subset of the values returned when no $entity is provided.
   *
   * @param \Drupal\Core\Field\FieldStorageDefinitionInterface $definition
   *   The field storage definition.
   * @param \Drupal\Core\Entity\FieldableEntityInterface|null $entity
   *   (optional) The entity context if known, or NULL if the allowed values are
   *   being collected without the context of a specific entity.
   * @param bool &$cacheable
   *   (optional) If an $entity is provided, the $cacheable parameter should be
   *   modified by reference and set to FALSE if the set of allowed values
   *   returned was specifically adjusted for that entity and cannot not be reused
   *   for other entities. Defaults to TRUE.
   *
   * @return array
   *   The array of allowed icon values. Keys of the array should be directly
   *   usable as CSS class for icon font usage. All existing icon font
   *   descriptions (as yml files) will be crawled in general use cases.
   *   In the context of a specific entity, it may be restricted to a single
   *   icon font - if the given entity's author has a theme restriction set.
   */
  public static function getAllowedValues(FieldStorageDefinitionInterface $definition = NULL, FieldableEntityInterface $entity = NULL, &$cacheable = TRUE) {
    $theme = \Drupal::config('system.theme')->get('default') ?: 'unsinn_base_theme';
    $reader = new IconYamlReader();
    return $reader->getOptions($theme, TRUE);
  }

  /**
   * Provides a list of icon options not prefixed by its font name.
   *
   * @return array
   *    Provides a list of icon options not prefixed by its font name.
   */
  public static function getAllowedValuesUnprefixed() {
    $theme = \Drupal::config('system.theme')->get('default') ?: 'unsinn_base_theme';
    $reader = new IconYamlReader();
    return $reader->getOptions($theme, FALSE);
  }

}
