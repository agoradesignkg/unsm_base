<?php

namespace Drupal\unsm_base\Util;

use Drupal\Component\Serialization\Yaml;

class IconYamlReader {

  protected $mapping = [];

  /**
   * Initializes the mapping by loading the icon fonts mapping file.
   *
   * @param string $parent_directory
   *   The parent directory, where the categories yml file is location. Defaults
   *   to 'private://iconfonts/'.
   *
   * @return bool
   *   TRUE, if the icon font mapping file was found and loaded successfully,
   *   FALSE otherwise. Empty or invalid mapping files will also lead to FALSE.
   */
  public function init($parent_directory = 'private://iconfonts/') {
    $parent_directory .= (substr($parent_directory, -1) == '/' ? '' : '/');
    $yaml = sprintf('%s/icons.yml', $parent_directory);
    if (!file_exists($yaml)) {
      return FALSE;
    }
    $this->mapping = Yaml::decode(file_get_contents($yaml));
    return !empty($this->mapping);
  }

  /**
   * Returns the mapped icons for usage in an options list.
   *
   * @param string|null $theme
   *   The theme to load the icons for. If left empty, options for all available
   *   fonts will be loaded.
   * @param bool $use_prefix
   *   Whether or not the keys should be prefixed with the font name prefix.
   *   Defaults to TRUE.
   *
   * @return array
   *   The icons prepared for usage in an options list.
   */
  public function getOptions($theme = NULL, $use_prefix = TRUE) {
    if (empty($this->mapping)) {
      $this->init();
    }
    if (!empty($theme)) {
      if (!isset($this->mapping[$theme])) {
        return [];
      }
      return $this->getSingleFontOptions($this->mapping[$theme], $use_prefix);
    }
    else {
      $options = [];
      foreach ($this->mapping as $font) {
        $font_name = $font['name'];
        $options[$font_name] = $this->getSingleFontOptions($font, $use_prefix);
      }
      return $options;
    }
  }

  /**
   * Returns the mapped icons for a single font.
   *
   * @param array $font
   *   The theme/font to load the icons for (as defined in the yml file).
   * @param bool $use_prefix
   *   Whether or not the keys should be prefixed with the font name prefix.
   *   Defaults to TRUE.
   *
   * @return array
   *   The icons prepared for usage in an options list.
   */
  protected function getSingleFontOptions(array $font, $use_prefix = TRUE) {
    $options = [];
    $prefix = $font['prefix'];
    foreach ($font['icons'] as $icon) {
      $key = $use_prefix ? sprintf('%s-%s', $prefix, $icon['id']) : $icon['id'];
      $options[$key] = $icon['label'];
    }
    return $options;
  }

}
