<?php

/**
 * Implements hook_views_data_alter().
 */
function unsm_base_views_data_alter(&$data) {
  $data['taxonomy_term_field_data']['term_alias'] = [
    'title' => t('Term alias'),
    'help' => t('Relate terms based on the suffix of their url alias.'),
    'real field' => 'tid',
    'argument' => [
      'title' => t('Term alias'),
      'id' => 'term_alias',
    ],
  ];
}
